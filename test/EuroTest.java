import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by bmoreira on 05/05/17.
 */
public class EuroTest {

    @Test
    public void shouldCreateTwoEuroWithValue10AndGetTrue(){
        assertEquals(new Euro(10), new Euro(10));
    }

    @Test
    public void shouldCompareEuro5AndEuro10AndGetFalse() {
        assertNotEquals(new Euro(5), new Euro(10));
    }

    @Test
    public void shouldCompareEuroWithValue5WithNull(){
        assertNotEquals(new Euro(5), null);
    }

    @Test
    public void shouldCompareEure10WithDifferentTypeAndGetFalse() throws Exception {
        assertNotEquals(new Euro(10), new Object());
    }

    @Test
    public void shouldCompareEuro10WithEuro7PlusEuro3AndGetTrue() throws Exception {
        assertEquals(new Euro(10),new Euro(7).add(new Euro(3)));
    }

    @Test
    public void shouldCompareEuro10WithEuro5PlusEuro2AndGetFalse() throws Exception {
        assertNotEquals(new Euro(10),new Euro(5).add(new Euro(2)));
    }

}