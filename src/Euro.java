/**
 * Created by bmoreira on 05/05/17.
 */
public class Euro {
    private final int amount;

    public Euro(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Euro euro = (Euro) o;

        return amount == euro.amount;
    }

    @Override
    public int hashCode() {
        return amount;
    }

    public Euro add(Euro euro) {
        return new Euro(this.amount+euro.amount);
    }
}
